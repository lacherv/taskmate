import { useState } from "react";
import "./App.css";
import Footer from "./components/Footer";
import Header from "./components/Header";
import TaskList from "./components/TaskList";

const App = () => {
  const info = "Random"
  return (
    <div className="App">
      <Header />
      <TaskList />
      <Footer />
    </div>
  );
};

export default App;
