import { useState } from "react";
import BoxCard from "./BoxCard";
import TaskCard from "./TaskCard";
import "./tasklist.css";

const TaskList = () => {
  const [tasks, setTasks] = useState([
    { id: 5271, name: "Records React lectures", completed: true },
    { id: 7825, name: "Edit lectures", completed: false },
    { id: 8391, name: "Watch lectures", completed: false },
  ]);

  const [show, setShow] = useState(true);

  const handleDelete = (id) => {
    setTasks(tasks.filter((task) => task.id !== id));
  };

  return (
    <section className="tasklist">
      <h1>Task List</h1>
      <ul>
        <button className="trigger" onClick={() => setShow(!show)}>
          Toggle
        </button>
        {show &&
          tasks.map((task) => (
            <TaskCard key={task.id} task={task} handleDelete={handleDelete} />
          ))}
      </ul>
      <BoxCard result="success">
        <p className="title">Offer Notification</p>
        <p className="description">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, est!
        </p>
      </BoxCard>
      <BoxCard result="warning">
        <p className="title">Cookie Notification</p>
        <p className="description">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, est!
        </p>
      </BoxCard>
    </section>
  );
};

export default TaskList;
